// Input :
// Input merupakan semua jenis karakter dengan maksimum 100 karakter.
// Output :
// Untuk setiap input urutkan berdasarkan posisi munculnya karakter tersebut, karakter
// spasi diabaikan.
// Sample Case :
// Input Output
// hello world hellloowrd
// how much howmuch
function sort(str){
    if(str.length <= 100){
        let arr = str.split('').filter((el)=>el!==" ")
        let setArr = [...new Set(arr)];
        let result = [];
        for (let i = 0; i < setArr.length; i++) {
            for (let j = 0; j < arr.length; j++) {
                if(setArr[i] === arr[j]){
                    result.push(setArr[i]);
                }
            }
        }
        return result.join('');
    }
}
console.log(sort('hello world'));
console.log(sort('how much'));
console.log(sort('asdfkjashdfjlasfnvcnmvcnjfhurejafldjhfllsdjfasdfajhsdfljasdfkajsdfklasdfasljksdfoiwuerljhsdfoiwerljhsdfoiwerljsdfudfsadfafasdfasdf'));

