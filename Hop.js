// 1 2 Hop! (30 Poin)

// Lie adalah seorang guru TK yang baik. Dia telah mengajar anak-anak untuk
// menghitung angka dari 1 sampai 9 menggunakan sebuah permainan sederhana. Berikut
// adalah permainan: Lie akan mengatakan nomor ke anak-anak. Untuk nomor yang
// dikatakannya, dia ingin anak-anak untuk menghitung dari 1 sampai N untuk N kali,
// dengan jumlah setiap N diganti dengan berteriak "hop!" Sebagai contoh: Jika ia
// mengatakan 3, maka anak akan menghitung: "1 2 hop! 1 2 hop! 1 2 hop!" Jika ia
// mengatakan 4, maka anak akan menghitung: "1 2 3 hop! 1 2 3 hop! 1 2 3 hop! 1 2 3
// hop!" Namun jika dia mengatakan setiap nomor lebih besar dari 9, maka anak akan
// berteriak: "Apa?" Sekarang, tulis sebuah program untuk mensimulasikan pengajaran Lie.
// Input :
// Setiap kasus uji berisi satu bilangan bulat N (1 <= N <= 20), jumlah yang Lie katakan.
// Output :
// Untuk setiap kasus, cetak teriakan anak-anak dalam satu baris. Setiap penghitungan
// harus dipisahkan dengan spasi tunggal (lihat contoh output).
// Sample Case :
// Input Output
// 3 1 2 hop! 1 2 hop! 1 2 hop!
function Hop(num) {
    if(num >= 1 && num <= 20){
        let hop = 'hop!';
        let result = '';
        if(num > 9 && num <= 20){
            result = 'Apa?';
            return result;
        }
        else{
            for (let i = 1; i <= num; i++) {
                i === num ? 
                    result += `${hop} ` : 
                    result += `${i} `
            }
        }
        return result.repeat(num).trim();
    }
}
console.log(Hop(3));
console.log(Hop(4));
console.log(Hop(15));
