// Kamus Panda (40 Poin)

// Panda Buku, setelah membaca sekian banyak kamus (berbahasa Panda),
// memutuskan untuk menciptakan sebuah kamus Panda yang baru. Panda Buku
// mengajukan suatu cara pengurutan kata dalam kamusnya, yaitu berdasarkan jenis
// karakter unik yang ada pada kata. Walaupun rumit, cara ini dipakainya agar tidak
// dituduh mencontek kamus buatan Kutu Buku, yang terurut secara alfabetis.
// Berikut ini adalah beberapa contoh cara menghitung jenis karakter unik dari
// sebuah kata yang dilakukan oleh Panda Buku: “lalala” terdiri dari 2 jenis karakter, yaitu
// “l” dan “a‟. “panda” terdiri dari 4 jenis karakter, yaitu “p‟, “a‟, “n‟, dan “d‟. Adapun, dalam
// bahasa Panda, semua kata hanya terdiri dari huruf kecil dari “a‟ sampai “z‟. Semua
// karakter lain tidak dihitung sebagai bagian dari kata.
// Karena Panda Buku sudah agak rabun dan kata yang dimilikinya amat banyak,
// anda diminta membuat program untuk menghitung jenis karakter unik untuk setiap kata
// yang diberikan oleh Panda Buku, agar ia dapat segera menyusun kamusnya.
// Input :
// Input hanya berisi satu kata. Setiap kata terdiri dari maksimal 200 karakter.
// Output :

// Output merupakan jenis karakter unik yang dimiliki setiap kata.
// Sample Case :
// Input Output
// lalala 2
// How long 5
// dst...
function panda(str){
    let arr = [...new Set(str.split(''))].filter((el)=> {
        let pattern = /^[a-z]+$/ 
        return el !== " " && pattern.test(el)}).length;
    return arr;
}
console.log(panda("panda"));
console.log(panda("lalala"));
console.log(panda("How long"));
// console.log(panda("panda"));
